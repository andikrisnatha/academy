<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('title');
            $table->string('guest_email');
            $table->date('departure_date');
            $table->date('arrival_date');
            $table->time('arrival_time')->nullable();
            $table->string('carier_code');
            $table->string('eta');
            $table->string('preference_bed');
            $table->string('preference_food');
            $table->string('transfer');
            $table->string('special_request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
