<?php

namespace Database\Factories;

use App\Models\Topic;
use Illuminate\Database\Eloquent\Factories\Factory;

class TopicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Topic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8]),
            'topic_name' => $this->faker->sentence($nbWords = 7, $variableNbWords = true),
            'topic_detail' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'training_method' => $this->faker->randomElement(['class', 'briefing', 'role-play', 'onlive-essay', 'online-webinar', 'one-on-one']),
            'training_date' => $this->faker->dateTimeThisYear($max = 'now', $timezone = null),
            'training_duration' => $this->faker->numberBetween($min = 30, $max = 150),
            'training_time' =>$this->faker->time($format = 'H:i', $max = 'now'),
            'training_location' =>$this->faker->randomElement(['zoom', 'office', 'training room', 'lobby', 'meeting room']),
            'status' =>$this->faker->randomElement(['close', 'open']),
        ];
    }
}
