<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Andi K', 
            'username' => 'andi', 
            'nik' => '123456', 
            'email' => 'andi@mail.com',
            'password' => bcrypt('12345678')
        ]);
  
        $role = Role::create(['name' => 'Admin']);
   
        $permissions = Permission::pluck('id','id')->all();
  
        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);

        // // generate trainers
        $trainerRole = Role::create(['name' => 'trainer']);
        $trainerRole->givePermissionTo('topic-create');

        $trainers = User::factory()->count(10)->create()->each(function ($trainer) {
            $trainer->assignRole('trainer');
        });
   
    }
}
