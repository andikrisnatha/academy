<?php

namespace Database\Seeders;

use App\Models\Topic;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainer = User::role('trainer')->first();
        $topic = Topic::create([
            'topic_name' => 'Test',
            'topic_detail' => 'Test Detail',
            'training_date' => Carbon::now(),
            'training_time' => Carbon::now(),
            'training_location' => 'Denpasar',
            'training_duration' => '180',
            'status' => 'active',
        ]);
        $trainer->topics()->save($topic);
    }
}
