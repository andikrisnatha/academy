<div wire:poll.250ms>
    <div class="grid grid-cols-2 gap-4">
        <div class="rounded bg-white shadow-md p-3 text-center">
            <h2 class="text-4xl mb-2">{{ $this->totalRemaining }}</h2>
            Remaining Jobs
        </div>
        <div class="rounded bg-white shadow-md p-3 text-center">
            <h2 class="text-4xl mb-2">{{ $this->totalFailed }}</h2>
            Failed Jobs
        </div>
    </div>
</div>
