

@extends('layouts.apps')


@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

  <div class="mt-1">
    <div class="min-h-screen bg-gray-100 py-2 flex flex-col justify-center mx-auto sm:py-2">
      @foreach ($surveys as $survey)
      <div class="p-2 grid grid-cols-1 sm:grid-cols-4 gap-"4>
        <div class="divide-white divide-y p-4 overflow-hidden rounded-3xl bg-indigo-500 border shadow-sm text-white hover:bg-green-500 hover:rotate-1 transition-transform">
          <div>
            <p class="text-white text-xl tracking-wide">{{ $survey->title }} {{ $survey->name }}</p>
            <p class="text-xs text-white">{{ $survey->arrival_date->format('M d, y') }} - {{ $survey->departure_date->format('M d, y') }}</p>
          </div>
          <div class="divide-y w-full divide-blue-200"></div>
          <div class="mt-3">
            <span class="text-sm text-white">
              the guest will be arriving by {{ $survey->carier_code }} ETA {{ $survey->eta }}. <br>
              They will be using {{ $survey->transfer }} from the airport. <br>
              thay are requesting {{ $survey->preference_bed }} bed for their bedding preference <br>
              for food, they are <strong>{{ $survey->preference_food }}</strong> <br>
              thei special request is {{ $survey->special_request }}
    
            </span>
          </div>
        </div>
      </div>
      {!! $surveys->links() !!}
      @endforeach
    </div>
  </div>

@endsection