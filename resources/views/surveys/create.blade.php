<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
  <title>The ANVAYA Beach Resort Bali - Arrival survey</title>
</head>
<body class="bg-gray-100 flex bg-local" style="background: url('./assets/svg/architect.svg')">
  <div class="bg-gray-100 mx-auto max-w-6xl py-20 px-12 lg:px-24 shadow-xl mb-24">
    <div class="flex item-center justify-center">
      <img src="https://theanvayabali.com/frontend/images/material/logo.png" alt="logo">
      <br>
    </div>
    <div class="p-6">
      <p class="w-full">Dear {{ $mailing->guest_name }},</p>
      <p class="w-full">To make your stay the most memorable one, kindly fill this pre-arrival form</p>
    </div>

    {{-- erorr message --}}
    @if ($errors->any())
    <div class="bg-blue-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative" role="alert">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
      <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
        <svg class="fill-current h-6 w-6 text-blue-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
      </span>
    </div>
    @endif


    {{-- input form --}}
    <form action="{{ route("surveys.store") }}" method="POST">
      @csrf
      @method('POST')

      <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
        <div class="-mx-3 md:flex mb-6">
          <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="name">
              Name
            </label>
            <input name="name" value="{{ $mailing->guest_name }}" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="name" type="text" placeholder="Name">
          </div>
          <div class="md:w-1/2 px-3">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="title">
              Title
            </label>
            <select name="title" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="title" type="text" placeholder="">
              <option value="">--please select--</option>
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </select>
            
          </div>
        </div>
        <div class="-mx-3 md:flex mb-6">
          <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="guest_email">
              Email
            </label>
            <input name="guest_email" value="{{ $mailing->guest_email }}" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="guest_email" type="text" placeholder="name@mail.com">
          </div>
        </div>
        <div class="-mx-3 md:flex mb-2">
          <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="arrival_date">
              Arrival Date
            </label>
            <div>
              <input name="arrival_date" value="{{ $mailing->arrival }}" type="date" class="w-full bg-gray-200 border border-gray-200 text-black text-xs py-3 px-4 pr-8 mb-3 rounded" id="departure_date">
            </div>
          </div>
          <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="departure_date">
              Departure Date
            </label>
            <div>
              <input name="departure_date" type="date" class="w-full bg-gray-200 border border-gray-200 text-black text-xs py-3 px-4 pr-8 mb-3 rounded" id="departure_date">
            </div>
          </div>
          <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="carier_code">
              Carier Code
            </label>
            <div>
              <input name="carier_code" type="text" class="w-full bg-gray-200 border border-gray-200 text-black text-xs py-3 px-4 pr-8 mb-3 rounded" id="carier_code" placeholder="your flight detail">
            </div>
          </div>
          <div class="md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="eta">
              ETA
            </label>
            <div>
              <input name="eta" type="text" class="w-full bg-gray-200 border border-gray-200 text-black text-xs py-3 px-4 pr-8 mb-3 rounded" id="carier_code" placeholder="flight time">
            </div>
          </div>
        </div>
        <div class="-mx-3 md:flex mb-6">
          <div class="md:w-full px-3">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="transfer">
              Airport Transfer
            </label>
            <select name="transfer" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="transfer" type="text">
              <option value="">--please select--</option>
              <option value="Innova 150K">Innova (up to 3 person) at IDR 150,000/car/way</option>
              <option value="Innova 150K">Hi-Ace (up to 10 person) at IDR 500,000/car/way</option>
            </select>
          </div>
          <div class="md:w-full px-3">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="preference_bed">
              Bedding Preferences
            </label>
            <select name="preference_bed" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="preference_bed" type="text">
              <option value="">--please select--</option>
              <option value="King">King Bed (1 Large Bed)</option>
              <option value="Twin">Twin Bed (2 separate Beds)</option>
            </select>
          </div>
        </div>
        <div class="-mx-3 md:flex mb-6">
          <div class="md:w-full px-3">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="preference_food">
              Food Preferences
            </label>
            <input name="preference_food" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="preference_food" type="text" placeholder="dietary, allergies...please specify">
          </div>
        </div>
        <div class="-mx-3 md:flex mb-6">
          <div class="md:w-full px-3">
            <label class="uppercase tracking-wide text-black text-xs font-bold mb-2" for="special_request">
              Special request
            </label>
          <input name="special_request" class="w-full bg-gray-200 text-black border border-gray-200 rounded py-3 px-4 mb-3" id="special_request" type="text" placeholder="please specify..">
          </div>
        </div>
        <div class="-mx-3 md:flex mt-2">
          <div class="md:w-full px-3">
            <button class="md:w-full bg-gray-900 text-white font-bold py-2 px-4 border-b-4 hover:border-b-2 border-gray-500 hover:border-gray-100 rounded-full">
              Submit
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</body>
</html>


