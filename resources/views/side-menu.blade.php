@can('user-create')
<div class="sidebar-heading">
    User Management
</div>

<!-- Nav Item - Users Collapse Menu -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers"
        aria-expanded="true" aria-controls="collapseUsers">
        <i class="fas fa-fw fa-user"></i>
        <span>Users</span>
    </a>
    <div id="collapseUsers" class="collapse" aria-labelledby="headingUsers" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('users.index') }}">All Users</a>
            <a class="collapse-item" href="{{ route('users.create') }}">Register New User</a>
        </div>
    </div>
</li>
@endcan

<!-- Nav Item - Roles Collapse Menu -->
@can('role-create')
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseRoles"
        aria-expanded="true" aria-controls="collapseRoles">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Roles</span>
    </a>
    <div id="collapseRoles" class="collapse" aria-labelledby="headingRoles"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('roles.index') }}">All Roles</a>
            <a class="collapse-item" href="{{ route('roles.create') }}">Create New Roles</a>
        </div>
    </div>
</li>
@endcan

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Training
</div>

<!-- Nav Item - Training Topic Collapse Menu -->
@can('topic-create')
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTopic"
        aria-expanded="true" aria-controls="collapseTopic">
        <i class="fas fa-fw fa-pen"></i>
        <span>Topics</span>
    </a>
    <div id="collapseTopic" class="collapse" aria-labelledby="headingTopic" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('topics.index') }}">My Topics</a>
            <a class="collapse-item" href="{{ route('topics.all') }}">All Topics</a>
            <a class="collapse-item" href="{{ route('topics.create') }}">Create New Topic</a>
        </div>
    </div>
</li>
@endcan


<!-- Nav Item - Absence Session Collapse Menu -->
@can('session-create')
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSession"
        aria-expanded="true" aria-controls="collapseSession">
        <i class="fas fa-fw fa-book"></i>
        <span>Training Sessions</span>
    </a>
    <div id="collapseSession" class="collapse" aria-labelledby="headingSession"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('sessions.index') }}">My Sessions</a>
            <a class="collapse-item" href="{{ route('sessions.create') }}">New Absence Session</a>
        </div>
    </div>
</li>
@endcan

<!-- Divider -->
<hr class="sidebar-divider">
<!-- Heading -->
<div class="sidebar-heading">
    Survey
</div>

<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMailing"
        aria-expanded="true" aria-controls="collapseSession">
        <i class="fas fa-fw fa-book"></i>
        <span>Mailing</span>
    </a>
    <div id="collapseMailing" class="collapse" aria-labelledby="headingMailing"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('mailings.index') }}">Request</a>
            <a class="collapse-item" href="{{ route('surveys.index') }}">Response</a>
            <a class="collapse-item" href="{{ route('mailings.create') }}">New</a>
            <a class="collapse-item" href="{{ route('newsletter.send') }}">Email Blast</a>
        </div>
    </div>
</li>
<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
