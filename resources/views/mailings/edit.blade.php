@extends('layouts.apps')

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('topics.update', $topic->id) }}" method="PATCH">
    @csrf
    @method('PATCH')
    
    <div class="mt-10 sm:mt-0">
        <div class="md:grid md:grid-cols-3 md:gap-6">
            <div class="md:col-span-1">
                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">Information</h3>
                    <p class="mt-1 text-sm text-gray-600">
                        Training topic should be created only by related trainer because it will colerate to trainer's KPI. <br> <br>
                        <strong>Training Status</strong> defines availability of the related topics. <strong>Topic Close</strong> means trainee will not be abel to fill the absence of given topic.
                    </p>
                </div>
            </div>
            <div class="mt-5 md:mt-0 md:col-span-2">
                <form action="#" method="POST">
                    <div class="shadow overflow-hidden sm:rounded-md">
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <div class="grid grid-cols-6 gap-6">
                                <div class="col-span-6 sm:col-span-3">
                                    <label for="topic_name" class="block text-sm font-medium text-gray-700">Topic Title</label>
                                    <input type="text" name="topic_name" value="{{ $topic->topic_name }}" id="topic_name" autocomplete="topic_name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Training Topic">
                                </div>
                                
                                <div class="col-span-6 sm:col-span-3">
                                    <label for="training_method" class="block text-sm font-medium text-gray-700">Method</label>
                                    <select type="text" name="training_method" value="{{ $topic->training_method }}" id="training_method" autocomplete="training_method" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                        <option value="class">Class</option>
                                        <option value="briefing">Briefing</option>
                                        <option value="role-play">Role-Play</option>
                                        <option value="online-essay">Online-Essay</option>
                                        <option value="online-webinar">Online-Webinar</option>
                                        <option value="one-on-one">One-on-One</option>
                                    </select>
                                </div>
                                <div class="col-span-6">
                                    <label for="topic_detail" class="block text-sm font-medium text-gray-700">Topic Detail</label>
                                    <textarea type="text" name="topic_detail" value="{{ $topic->topic_detail }}" id="topic_detail" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Further detail related to the topic"></textarea>
                                </div>
                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                    <label for="training_date" class="block text-sm font-medium text-gray-700">Training Date</label>
                                    <input type="date" name="training_date" value="{{ $topic->training_date }}" id="training_date" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>
                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                    <label for="training_time" class="block text-sm font-medium text-gray-700">Training Time</label>
                                    <input type="time" name="training_time" value="{{ $topic->training_time }}" id="training_time" autocomplete="training_time" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>
                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                    <label for="training_duration" class="block text-sm font-medium text-gray-700">Duration (minutes)</label>
                                    <input type="number" name="training_duration" value="{{ $topic->training_duration }}" id="training_duration" autocomplete="training_duration" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Minimal 15 minutes">
                                </div>
                                <div class="col-span-6 sm:col-span-3">
                                    <label for="training_location" class="block text-sm font-medium text-gray-700">Location</label>
                                    <input type="text" name="training_location" value="{{ $topic->training_location }}" id="training_location" autocomplete="training_location" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Location or training link if the session is online">
                                </div>
                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                    <label for="status" class="block text-sm font-medium text-gray-700">Status</label>
                                    <select type="text" name="status" value="{{ $topic->status }}" id="status" autocomplete="status" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" placeholder="Minimal 15 minutes">
                                        <option value="open">Open</option>
                                        <option value="close">Close</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</form>


@endsection

