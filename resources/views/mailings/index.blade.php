

@extends('layouts.apps')


@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="flex flex-col">
    <div class="my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="table-auto w-full">
                    <thead class="bg-gray-50">
                        <tr>
                            <th scope="col" class="px-3 py-1 text-left text-xs font-medium text-gray-500 uppercase">
                                No
                            </th>
                            <th scope="col" class="px-3 py-1 text-left text-xs font-medium text-gray-500 uppercase">
                                Guest Name
                            </th>
                            <th scope="col" class="px-3 py-1 text-left text-xs font-medium text-gray-500 uppercase">
                                Arrival
                            </th>
                            <th scope="col" class="px-3 py-1 text-left text-xs font-medium text-gray-500 uppercase">
                                Email
                            </th>
                            <th scope="col" class="px-3 py-1 text-left text-xs font-medium text-gray-500 uppercase">
                                Submitted by
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($mailings as $mailing )
                        <tr>
                            
                            <td class="px-4 py-2  text-sm text-gray-900">
                                {{ $loop->index + 1 }}
                            </td>
                            <td class="px-4 py-2">
                                <div class="text-sm text-gray-900">{{ $mailing->guest_name }}</div>
                            </td>
                            <td class="px-4 py-2  text-sm text-gray-900">
                                {{ $mailing->arrival }}
                            </td>
                            <td class="px-4 py-2  text-sm text-gray-900">
                                {{ $mailing->guest_email }}
                            </td>
                            <td class="px-4 py-2  text-sm text-gray-900">
                                {{ $mailing->user->name }}
                            </td>
                            {{-- <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <form action="{{ route('topics.destroy', $topic->id) }}" method="post" class="flex">
                                    <a href="{{ route('topics.edit', $topic->id) }}" class="" alt='edit'>
                                        <i>
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="#60A5FA">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                            </svg>
                                        </i>
                                    </a>
                                    
                                    @csrf
                                    @method('DELETE')
                                    
                                    <button type="submit" class="">
                                        <i>
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="#DB2777">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                            </svg>
                                        </i>
                                    </button>
                                    
                                </form>
                            </td> --}}
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            {!! $mailings->links() !!}
        </div>
    </div>
</div>

@endsection