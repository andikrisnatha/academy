@component('mail::message')
# Survey Filled

A survey has been filled by {{ $survey->name }}

<dl>
  <dt>Name</dt>
  <dd>{{ $survey->title }} {{ $survey->name }}</dd>
  <dt>Email</dt>
  <dd>{{ $survey->email }}</dd>
  <dt>Special Request</dt>
  <dd>{{ $survey->special_request }}</dd>
  <dt>Arrival</dt>
  <dd>{{ $survey->arrival_date }} {{ $survey->arrival_time }}</dd>
  <dt>Departure</dt>
  <dd>{{ $survey->departure_date }}</dd>
  <dt>Carier Code</dt>
  <dd>{{ $survey->carier_code }}</dd>
  <dt>Estimated Time Arrival</dt>
  <dd>{{ $survey->eta }}</dd>
  <dt>Bed Preference</dt>
  <dd>{{ $survey->preference_bed }}</dd>
  <dt>Food Preference</dt>
  <dd>{{ $survey->preference_food }}</dd>
  <dt>Transfer</dt>
  <dd>{{ $survey->transfer }}</dd>
</dl>

Thanks,<br>
{{ config('app.name') }}
@endcomponent