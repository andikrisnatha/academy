@extends('layouts.apps')
@section('content')
<div class="flex w-full h-screen items-center justify-center bg-grey-lighter ">
  <form action="{{ route('newsletter.send')  }}" method="post" class="space-y-4" enctype="multipart/form-data">
    @csrf
    <div>
      <label for="subject">Subject</label>
      <input type="text" class="py-1 px-3 border border-gray-400 rounded bg-white text-gray-700 block" name="subject" id="subject">
    </div>
    {{-- <div>
      <label for="mailSource">CSV File</label>
      <input type="file" name="mailSource" id="mailSource" class="block">
    </div> --}}
    <label class="w-64 flex flex-col items-center px-4 py-6 bg-white text-blue-500 rounded-lg shadow-lg tracking-wide uppercase border border-blue-300 cursor-pointer hover:bg-blue hover:text-red-500">
      <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
        <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
      </svg>
      <span class="mt-2 text-base leading-normal hover:text-red-500 ">Select a CSV file</span>
      <input name="mailSource" id="mailSource" type='file' class="hidden" />
    </label>
    <label>
      <input type="checkbox" name="hasHeader" value="true">
      <span>CSV file has header</span>
    </label>
    <div>
      <button type="submit" class="py-2 px-4 rounded bg-whtie text-green-500 border-green-500 hover:bg-green-500 hover:text-white ">Blast Mail</button>
    </div>
  </form>
</div>
@endsection