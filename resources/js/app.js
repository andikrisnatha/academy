require("./bootstrap");

import Alpine from "alpinejs";

window.Alpine = Alpine;

Alpine.start();

window.onSelectTrainer = function (trainer) {
    const trainerId = trainer.value;
    const select = document.getElementById("topic_id");

    select.innerHTML = "";

    window.axios
        .get("/api/topics", {
            params: {
                trainer_id: trainerId,
            },
        })
        .then(function (response) {
            const topics = response.data;
            topics.forEach((topic) => {
                const option = document.createElement("option");
                option.text = topic.topic_name;
                option.value = topic.id;
                option.dataset.detail = topic.topic_detail;
                select.appendChild(option);
            });

            select.onchange();
        });
};

window.onSelectTopic = function (topic) {
    window.axios.get(`/api/topics/${topic.value}`).then(function (response) {
        const topic = response.data;

        const trainingLocationContainer =
            document.getElementById("training_location");
        trainingLocationContainer.value = topic.training_location;

        const topicDetailContainer = document.getElementById("topic_detail");
        topicDetailContainer.value = topic.topic_detail;

        const trainingDateContainer = document.getElementById("training_date");
        trainingDateContainer.value = topic.training_date;

        const trainingTimeContainer = document.getElementById("training_time");
        trainingTimeContainer.value = topic.training_time;

        const trainingDurationContainer =
            document.getElementById("training_duration");
        trainingDurationContainer.value = topic.training_duration;

        const trainingMethodContainer =
            document.getElementById("training_method");
        trainingMethodContainer.value = topic.training_method;
    });
};
