<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'title',
        'guest_email',
        'departure_date',
        'arrival_date',
        'arrival_time',
        'carier_code',
        'eta',
        'preference_bed',
        'preference_food',
        'transfer',
        'special_request',
        
    ];

    protected $cast = [
        'departure_date' => 'date:M d, y',
        'arrival_date' => 'date:M d, y',
    ];

    protected $dates = [
        'departure_date',
        'arrival_date'
    ];

}
