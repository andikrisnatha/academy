<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Topic extends Model
{
    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function topics(): HasMany
    {
        return $this->hasMany(TrainingSession::class, 'topic_id');
    }

    protected $fillable = [
        'name', 'detail',
    ];

    protected $casts = [
        'training_date' => 'date:M d, y',
        'training_time' => 'datetime:H:i',
    ];
}
