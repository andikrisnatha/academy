<?php

namespace App\Http\Controllers;

use App\Mail\SurveyResponse;
use App\Models\Mailing;
use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Survey::orderBy('created_at', 'DESC')->paginate(10);

        return view('surveys.index', compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $hash = $request->get('u', '');
        $id = Crypt::decrypt($hash);
        $mailing = Mailing::find($id);
        return view('surveys.create', compact('mailing'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $survey = new Survey;
        $survey->name = $request->get('name');
        $survey->title = $request->get('title');
        $survey->arrival_date = $request->get('arrival_date');
        $survey->departure_date = $request->get('departure_date');
        $survey->arrival_time = $request->get('arrival_time');
        $survey->carier_code = $request->get('carier_code');
        $survey->eta = $request->get('eta');
        $survey->preference_bed = $request->get('preference_bed');
        $survey->preference_food = $request->get('preference_food');
        $survey->transfer = $request->get('transfer');
        $survey->guest_email = $request->get('guest_email');
        $survey->special_request = $request->get('special_request');
        $survey->save();

        Mail::to(config('email.reception'))->send(new SurveyResponse($survey));

        return redirect()->route('surveys.thanks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function thanks()
    {
        return view('surveys.thanks');
    }
}
