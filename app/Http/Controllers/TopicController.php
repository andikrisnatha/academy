<?php
    
namespace App\Http\Controllers;

use App\Http\Requests\TopicRequest;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:topic-list|topic-create|topic-edit|topic-delete', ['only' => ['index','show']]);
         $this->middleware('permission:topic-create', ['only' => ['create','store']]);
         $this->middleware('permission:topic-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:topic-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(5);
        $pageTitle = 'My Topic';
        return view('topics.all', compact('topics', 'pageTitle'));
    }
    
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $topics = Topic::orderBy('created_at', 'DESC')->paginate(5);
        $pageTitle = 'All Topic';
        return view('topics.all', compact('topics', 'pageTitle'));
    }
    

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topics.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TopicRequest $request)
    {
        $topic = new Topic;
        $topic->user_id = Auth::user()->id;
        $topic->topic_name = $request->get('topic_name');
        $topic->topic_detail = $request->get('topic_detail');
        $topic->training_method = $request->get('training_method');
        $topic->training_date = $request->get('training_date');
        $topic->training_time = $request->get('training_time');
        $topic->training_location = $request->get('training_location');
        $topic->training_duration = $request->get('training_duration');
        $topic->status = $request->get('status');
        $topic->save();
    
        return redirect()->route('topics.index')
                        ->with('success','Topic created successfully.');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic  $topic)
    {
        return view('topics.show',compact('topic'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic  $topic)
    {
        return view('topics.edit', compact('topic'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(TopicRequest $request, Topic $topic)
    {    
        $topic->topic_name = $request->get('topic_name');
        $topic->topic_detail = $request->get('topic_detail');
        $topic->training_method = $request->get('training_method');
        $topic->training_date = $request->get('training_date');
        $topic->training_time = $request->get('training_time');
        $topic->training_location = $request->get('training_location');
        $topic->status = $request->get('status');
        $topic->save();
    
        return redirect()->route('topics.index')
                        ->with('success','Topic updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic  $topic)
    {
        try {
            $topic->delete();
        } catch (\Throwable $th) {
            abort(500);
        }
    
        return redirect()->route('topics.index')
                        ->with('success','Topic deleted successfully');
    }

    public function fetch(Request $request)
    {
        $trainerId = $request->get('trainer_id');

        $topics = Topic::where('user_id', $trainerId)->where('status', 'open')->get();

        return $topics;
    }

    public function fetchInfo(Topic $topic)
    {
        return $topic;
    }
}