<?php

namespace App\Http\Controllers;

use App\Mail\Newsletter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    public function index()
    {
        return view('newsletter.index');
    }

    public function send(Request $request)
    {
        $subject = $request->get('subject', '');
        $hasHeader = $request->get('hasHeader', false);
        $mailSource = $request->file('mailSource')->getRealPath();
        $data = array_map('str_getcsv', file($mailSource));

        if ($hasHeader) {
            array_shift($data);
        }

        DB::table('failed_jobs')->truncate();

        foreach ($data as $person) {
            $name = $person[0];
            $email = $person[1];
            if (filter_var( $email, FILTER_VALIDATE_EMAIL )) {
                Mail::to($email)->send(new Newsletter($name, $subject));
            }
        }

        return redirect()->route('newsletter.progress');
    }

    public function progress()
    {
        return view('newsletter.monitor');
    }

}
