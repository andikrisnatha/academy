<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class MailProgress extends Component
{
    public function render()
    {
        return view('livewire.mail-progress');
    }

    public function getTotalRemainingProperty() {
        return DB::table('jobs')->count();
    }

    public function getTotalFailedProperty() {
        return DB::table('failed_jobs')->count();
    }
}
