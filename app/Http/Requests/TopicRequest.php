<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'topic_name' => 'required|max:30',
            'training_method' => 'required',
            'training_date' => 'required',
            'training_time' => 'required',
            'training_location' => 'required',
            'training_duration' => 'required|numeric|max:960',
        ];
    }

    public function messages()
    {
        return [
            'topic_name' => 'Topic Name',
            'training_method' => 'Training Method',
            'training_date' => 'Training Date',
            'training_time' => 'Training Time',
            'training_location' => 'Training Location',
            'training_duration' => 'Duration',
        ];
    }
}
