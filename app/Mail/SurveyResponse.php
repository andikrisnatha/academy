<?php

namespace App\Mail;

use App\Models\Survey;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SurveyResponse extends Mailable
{
    use Queueable, SerializesModels;

    private $survey;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($address = 'anvayabali@outlook.com', $name = "ANVAYA")
                    ->markdown('emails.survey-response')
                    ->subject('A Survey Has Been Filled')
                    ->with([ 'survey' => $this->survey ]);
    }
}
