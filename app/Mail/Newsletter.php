<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Newsletter extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $name;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $subject)
    {
        $this->name = $name;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($address = 'newsletter@theanvayabali.com', $name = "Anvaya Bali Newsletter")
                    ->view('emails.newsletter')
                    ->subject($this->subject)
                    ->with([ 'name' => $this->name ]);
    }
}

// afom@theanvayabali.com
// fom@theanvayabali.com
