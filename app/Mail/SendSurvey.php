<?php

namespace App\Mail;

use App\Models\Mailing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class SendSurvey extends Mailable
{
    use Queueable, SerializesModels;

    private $hash;
    private $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Mailing $mailing)
    {
        $this->hash = Crypt::encrypt($mailing->id);
        $this->name = $mailing->guest_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($address = 'anvayabali@outlook.com', $name = "ANVAYA")
                    ->view('emails.survey-request')
                    ->subject('Pre-Arrival Questionaire')
                    ->with([ 'hash' => $this->hash, 'name' => $this->name ]);
    }
}
