<?php

use App\Http\Controllers\MailingController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\TrainingSessionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::resource('roles', RoleController::class);
Route::resource('mailings', MailingController::class);

Route::resource('surveys', SurveyController::class)->only('create', 'store');
Route::get('surveys/thanks', [SurveyController::class, 'thanks'])->name('surveys.thanks');
Route::resource('sessions', TrainingSessionController::class);
Route::get('/topics/all', [TopicController::class, 'all'])->name('topics.all');

Route::get('anvayasurvey', [SurveyController::class, 'create'])->name('guest-survey');

Route::middleware(['auth:sanctum', 'verified'])->group(function(){

    Route::resource('users', UserController::class);
    Route::resource('surveys', SurveyController::class)->only('index');
    Route::resource('topics', TopicController::class);
    Route::get('/send-newsletter', [NewsletterController::class, 'index'])->name('newsletter.index');
    Route::post('/send-newsletter', [NewsletterController::class, 'send'])->name('newsletter.send');
    Route::get('/send-newsletter/progress', [NewsletterController::class, 'progress'])->name('newsletter.progress');
    Route::get('/', function(){
        return view('home');
    })->name('home');
});

Route::get('/dashboard', function(){
    return view('dashboard');
})->name('dashboard');