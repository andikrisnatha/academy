<?php

use App\Http\Controllers\API\TopicController as APITopicController;
use App\Http\Controllers\TopicController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
});
Route::get('/topics', [TopicController::class, 'fetch']);
Route::get('/topics/{topic}', [TopicController::class, 'fetchInfo']);

Route::resource('/topics', APITopicController::class);
